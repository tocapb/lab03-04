Con este lab aprendí más cosas sobre ensamblador, lo que vi más sencillo fue lo de cambiar de mayúscula a minúscula,
y aprendí lo util que es usar el ascii con el or para cambiar los valores, incluso mediante prueba y error me di cuenta que el 
or también cambiaba el cambio de linea o 0x0A, por lo que tuvimos que restarle uno al contador para evitar que se transformara ese 
caracter.

Con la parte pedir al usuario que ingrese el nombre, vi que es sencillo pues es solo saberse las llamadas al sistema necesarias y
saber donde se guardá el mensaje y el largo de este.

Con la parte de manejo de archivos también lo veo sencillo pues tambien es saberse las llamadas del sistema, lo más aburrido
de esto es estar leyendo a ver cuales son las que funcionan.

Para las partes de pausas no tengo mucha idea de como funciona, pero lo haré.

También aprendí mejor como hacer los diagramas de flujo, y no hacerlos tan específicos si no más generales.
