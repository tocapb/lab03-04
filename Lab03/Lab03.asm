SECTION .bss

contador: resb 4                                    ;se guardan 4 bytes para utilizar porque trabajaremos con registros de 32 bits.
contenido: resb 20                                ;se guardan 20 bytes del archivo a leer.
id resb 1                                           ;se guarda la dirección del archivo.

SECTION .data
; Definición de los mensajes y sus respectivos largos

mensaje: db "¿Cuál es su nombre?",0x0A
largo: equ $-mensaje

entrada: db "                    ", 0x0A             ;espacio reservado para guardar el nombre introducido por el usuario.
largo2: equ $-entrada                                ;aquí será almacenado el mismo después de pedirlo al usuario, máximo 19 caracteres con un enter.

mensaje3: db "procesando"
largo3: equ $-mensaje3

tiempos:                      ;utilizados para delay en el sistema.
    tv_sec  dd 0
    tv_usec dd 0

mensaje4: db " ."             ;definición de punto con espacio a utilizar para futura función de delay.
largo4: equ $-mensaje4

mensaje5: db 0x0A,0x0A,"Hola "
largo5: equ $-mensaje5

mensaje6: db 0x0A, "Chau", 0x0A
largo6: equ $-mensaje6

mensaje7:db "."
largo7: equ $-mensaje7

;acá se define el nombre del archivo
  archivo db "nombres.txt", 0h


SECTION .text

global _start

_start:

;todas las impresiones utilizarán DisplayText para ser llevadas a cabo.
;Se imprime "¿Cuál es su nombre?"

_imprimir_mensaje:
    mov edx,largo
    mov ecx,mensaje
    call DisplayText

;permite ingresar nombre de usuario
;Parecido a imprimir pero con otras llamadas al sistema
;utiliza 3 y 2 en lugar de 4 y 1, esto hace que lea del teclado
;utiliza registros eax, ebx, ecx y edx
;nombre de usuario queda en entrada
;no valida si nombre introducido es mayúscula
;cantidad de caracteres mas enter quedan guardados en eax

_entrada_nombre:
    mov eax, 3
    mov ebx, 2
    mov ecx, entrada
    mov edx, largo2
    int 80h
    mov dword[contador], eax   ;se mueve valor de eax a contador para utilizarlo mas adelante.

;se imprime procesando con DisplayText y maneja loop para introducir puntos con pausa.

_pausa_procesando:
    mov edx, largo3            ;imprime procesando primero con nuestra subrutina
    mov ecx, mensaje3
    call DisplayText
    mov ecx,dword [contador]   ;mueve el contador a ecx
    dec ecx                    ;se decrementa una vez para eliminar punto originado por enter


;Pausa de un segundo en el sistema

.pausa:
    push ecx
    mov dword [tv_sec], 1
    mov dword [tv_usec], 0
    mov eax, 162
    mov ebx, tiempos
    mov ecx, 0
    int 80h

;imprime un espacio y un punto en pantalla

.imprime_punto:
    mov edx, largo4
    mov ecx, mensaje4
    call DisplayText
    pop ecx
    loop .pausa

;imprime "Hola en pantalla"

_imprimir_mensaje5:
    mov ecx, mensaje5
    mov edx, largo5
    call DisplayText

;Convierte el texto del usuario a minúsculas
;Utiliza los registros eax y ecx
_convertir_minuscula:

    mov eax, entrada    ;eax tiene un puntero al inicio del nombre que el usuario ingresó
    mov ecx, [contador] ;ecx tiene el largo del nombre, se mueve a ecx para usarlo con loop
    dec ecx             ;se resta uno porque si no se tomaría en cuenta el cambio de linea
    jmp .convertir_minuscula_aux

.convertir_minuscula_aux:

    or dword[eax], 32               ;convierte cada letra a minúscula por medio del código ascii
    inc eax                         ;incrementa el puntero para pasar a la siguiente letra
    loop .convertir_minuscula_aux   ;El proceso se repite hasta que el contador llegue a 0

;imprime el nombre introducido ya convertido a minúsculas.

_imprimir_nombre:

    mov ecx, entrada
    mov edx, [contador]
    dec edx
    call DisplayText

.imprime_punto:

    mov edx, largo7
    mov ecx, mensaje7
    call DisplayText


;Se imprime "Chau" por medio de DisplayText
_imprimir_mensaje6:
    mov edx,largo6
    mov ecx,mensaje6
    call DisplayText


;Se maneja las opciones de crear, abrir , leer y escribir en el archivo llamándolas.

_manejo_archivos:

    call _abrir_archivo
    call _leer_archivo
    call _escribe_archivo
    call _cerrar_archivo
    call _fin



;Finaliza el programa
    _fin:
        mov ebx,0
        mov eax,1
        int 0x80

;Crear y editar archivos

;se abre el archivo por medio de sys_open que es una llamada al sistema.
;ecx son las banderas, en este caso de la bandera de creat,append,read and write todo en octal.
;el creat crea el archivo si no está creado, el append se dirige al final del archivo(si hay un nombre una linea abajo del nombre), y da una bandera para leer(read) y escribir(write).
;ebx donde se creara el archivo, si esta creado solo abre el archivo.
;eax llamamos a sys_open por medio de 8.
;edx son los permisos para el archivo, en este caso se da permiso de leer, escribir y ejecutar.
;Después de abrirse con éxito, en el registro eax se dejará el descriptor del archivo creado o abierto por lo que se guarda en id.

_abrir_archivo:

      mov ecx,2102o
      mov ebx,archivo
      mov edx,00700o
      mov eax,5
      mov dword[id],eax
      int 80h
      ret


;se lee el archivo por medio de sys_read que es una llamada al sistema.
;ecx se guarda lo que contiene del archivo.
;ebx se pone el descriptor de archivo.
;eax llamamos a sys_read por medio de 3.
;edx se pone la cantidad a leer del archivo.
;Después de leerse con éxito, en el registro eax se dejará el descriptor del archivo leído por lo que se guarda en id.

_leer_archivo:

      mov edx,[contador]
      mov ecx,contenido
      mov ebx,dword[id]
      mov eax,3
      mov dword[id],eax
      int 80h
      ret

;se escribe en el archivo por medio de sys_write que es una llamada al sistema.
;ecx se envía el mensaje a escribir (en esta caso entrada es el nombre que el usuario ingresó).
;edx se envía el largo del mensaje a escribir.
;ebx descriptor del archivo en donde se escribirá.
;eax llamamos a sys_write por medio de 4.

_escribe_archivo:

        mov edx,[contador]
        mov ecx,entrada
        mov ebx,dword[id]
        mov eax, 4
        int 80h
        ret




;se cierra el archivo por medio de sys_close que es una llamada al sistema.
;ebx descriptor de archivo a cerrar.
;eax, llamamos a sys_close por medio de 6.

_cerrar_archivo:

      mov ebx,dword[id]
      mov eax,6
      int 80h
      ret

;Subrutina DisplayText, utiliza registros eax y ebx.
;para utilizarse se necesita un puntero al inicio del mensaje en ecx.
;y el largo del mensaje en edx.

DisplayText:
    mov eax, 4
    mov ebx, 1
    int 80h
    ret
