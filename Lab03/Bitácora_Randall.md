Randall Zumbado Huertas
2019082664

Manejo de archivos

Para realizar el manejo de archivos realicé una investigación exhaustiva con respecto a la llamada el sistema.

Para esto además de investigar por aparte estas llamadas y como acoplarlas con los registros, utilicé más que todo el manual de usuario, además conocimientos que el profesor me indicó en las clases como el orden de los registros a la hora de leer el manual de usuario.

Un dato interesante que aprendí fue que hay diferentes direcciones de el manejo de estos archivos, por una parte tenemos la dirección relativa la cuál a lo que entendí es como realizar una dirección sin especificación, en este proyecto se utiliza este tipo de direción ya que independientemente en la computadora que se corra y tenga linux el archivo se creará en la carpeta que se ejecute el programa.

Se obtuvo ayuda de la siguiente página:
https://pastebin.com/wCNZs3RN
La cuál muestra los números en decimal de diferentes cosas, entre ellas las llamadas al sistema (Sys_Open, Sys_Read, Sys_Write ,Sys_Close) las cuales me ayudaron para averiguar como llamarlas en el registro eax, también tiene los números que funcionan para darle permisos al usuario y tambien las formas de abrir un archivo entre ellas O_Append, eso si tuve que pasarlas a octal y sumarlos para que funcionara. 

Con respecto al codigo de mis compañeros si entendí el funcionamiento solo que me faltó captar bien la idea del delay, pero le pregunté a mi compañero Jeremy y el me explico lo básico.

Gracias a el manual y consultas al profesor se pudo realizar el laboratorio completo y mejor aún en mi caso ya se manejar correctamente las llamadas al sistema.



