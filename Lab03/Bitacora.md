Llamadas que ejecutan los programas de aplicación para pedir algún servicio al Sistema operativo

hay dos tipos:
bloqueantes: cuando esperan un resultado
no bloqueantes: cuando no esperan resultado y se sigue ejecutando

algunos tipos sacados de internet:
Tipos de llamadas al sistemas
Control de procesos
Terminar (end), abortar (abort)
cargar (load), ejecutar (execute)
crear procesos (create process o submit job), terminar procesos (terminate process)
fork: inicia un nuevo proceso
exec:el programa se ejecuta
obtener atributos del proceso (get process attributes), definir atributos del proceso (set process attributes)
esperar para obtener tiempo (wait time)
esperar suceso (wait event), señalizar suceso (signal event)
asignar y liberar memoria.
llamada al sistema para liberar memoria (dump) (i45)
--------------------------------------------------------------
Administración de archivosa min
crear archivos (create), borrar archivos (delete)
abrir (open), cerrar (close)
leer (read), escribir (write), reposicionar (reposition)
obtener atributos del archivo, definir atributos del archivo.
move: mover archivos
copy: copiar archivos
---------------------------------------------------------------
Adminsitración de dispositivos
solicitar dispositivo (request), liberar dispositivo (release)
leer (read), escribir (write), reposicionar (reposition)
obtener atributos de dispositivo, definir atributos de dispositivo
conectar y desconectar dispositivos lógicamente.
Mantenimiento de la información
obtener la hora (time) o la fecha (date), definir la hora o la fecha
obtener datos del sistema, establecer datos del sistema
obtener los atributos de procesos, archivos o dispositivos
establer los atributos de procesos, archivos o dispositivos
Comunicaciones
crear, eliminar conexiones de comunicación
enviar, recibir mensajes
transferir información de estado
conectar y desconectar dispositivos remotos. (i43)

Para este lab debemos usar una llamada de Administración de archivos, seguramente crearlos para poner los nombres.


fuente de información:
http://hernandez-gonzalez-lenguajes.blogspot.com/2013/08/interrupciones-y-llamadas-al-sistema.html